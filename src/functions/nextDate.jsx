import React from "react";
import moment from "moment";
import Moment from "react-moment";
import store from "../store/store.js";
import { getBinDates } from "../actions/actionTypes.js";

export default function nextDate(type) {
  store.subscribe(() => store.getState());
  store.dispatch(getBinDates());

  moment.updateLocale("en", {
    invalidDate: "Service Suspended",
  });

  const date = (type) => (
    <div>
      {
        {
          recycling: (
            <Moment to={store.getState().recycling}>
              <Moment></Moment>
            </Moment>
          ),
          landfill: (
            <Moment to={store.getState().landfill}>
              <Moment></Moment>
            </Moment>
          ),
          garden: (
            <Moment to={store.getState().garden}>
              <Moment></Moment>
            </Moment>
          ),
          default: (
            <div>
              <p>Date Invalid</p>
            </div>
          ),
        }[type]
      }
    </div>
  );

  return <>{date(type)}</>;
}
