/*
 * action types
 */

export const BIN_DATES = "bins";
export const RECYCLING_BIN_SET = "recycling";
export const LANDFILL_BIN_SET = "landfill";
export const GARDEN_BIN_SET = "garden";

/*
 * action creators
 */

export const getBinDates = () => {
  return {
    type: BIN_DATES,
  };
};

export const getRecycling = () => {
  return {
    type: RECYCLING_BIN_SET,
    date: undefined,
  };
};

export const getGeneral = () => {
  return {
    type: LANDFILL_BIN_SET,
    date: undefined,
  };
};

export const getGarden = () => {
  return {
    type: GARDEN_BIN_SET,
    date: undefined,
  };
};
