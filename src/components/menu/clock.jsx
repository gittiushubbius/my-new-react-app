import React from "react";
import Typography from "@material-ui/core/Typography";

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({ date: new Date() });
  }
  render() {
    return (
      <div>
        <Typography marginLeft={"auto"}>
          {this.state.date.toLocaleTimeString()}
          {" - "}
          {this.state.date.toLocaleDateString()}
        </Typography>
      </div>
    );
  }
}

export default Clock;
