import React from "react";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import HealingIcon from "@material-ui/icons/HealingOutlined";
import HowToRegIcon from "@material-ui/icons/HowToReg";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

class Tester extends React.Component {
  render() {
    const anchor = "left";
    const toggleDrawer = (anchor, value) => (event) => {
      if (
        event.type === "keydown" &&
        (event.key === "Tab" || event.key === "Shift")
      ) {
        return;
      }
    };

    const list = (anchor) => (
      <div onKeyDown={toggleDrawer(anchor, this.props.toggle)}>
        <List>
          {["Bin Day", "Prescriptions", "Who Is In?"].map((text, index) => (
            <ListItem
              button
              key={text}
              onClick={() => this.props.select(index)}
            >
              <ListItemIcon>
                {
                  {
                    0: <DeleteOutlineIcon />,
                    1: <HealingIcon />,
                    2: <HowToRegIcon />,
                  }[index]
                }
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          {["Close Menu"].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>
                <ExitToAppIcon />
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </div>
    );

    return (
      <div>
        {
          <React.Fragment key={anchor}>
            <Drawer
              anchor={anchor}
              open={this.props.open}
              onClick={this.props.toggle}
            >
              {list(anchor)}
            </Drawer>
          </React.Fragment>
        }
      </div>
    );
  }
}

export default Tester;
