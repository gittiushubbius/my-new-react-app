import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import SideDrawer from "./drawerMenu.jsx";
import Clock from "./clock.jsx";

class Menu extends React.Component {
  constructor() {
    super();
    this.state = {
      drawerOpenStatus: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle = () => {
    this.setState((state) => ({
      drawerOpenStatus: !this.state.drawerOpenStatus,
    }));
  };

  render() {
    const useStyles = makeStyles((theme) => ({
      root: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "flex-start",
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      sideDrawer: {
        width: 500,
      },
    }));

    return (
      <div>
        <div>
          <AppBar position="static" className={useStyles.root}>
            <Toolbar>
              <IconButton
                edge="start"
                className={useStyles.menuButton}
                color="inherit"
                aria-label="menu"
                onClick={this.toggle}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" style={{ marginRight: "auto" }}>
                {this.props.title}
              </Typography>
              <Clock></Clock>
            </Toolbar>
          </AppBar>
        </div>
        <div className={useStyles.sideDrawer}>
          <SideDrawer
            open={this.state.drawerOpenStatus}
            toggle={this.toggle}
            select={this.props.selector}
          ></SideDrawer>
        </div>
      </div>
    );
  }
}

export default Menu;
