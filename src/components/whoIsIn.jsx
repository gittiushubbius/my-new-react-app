import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    paddingTop: 50,
  },
  paper: {
    padding: theme.spacing(4),
    textAlign: "center",
    color: theme.palette.text.secondary,
    border: "1px solid black",
  },
  binImage: {
    maxHeight: 50,
    paddingTop: 15,
  },
  alert: {
    color: "Red",
  },
  binBox: {
    marginLeft: "2%",
    marginRight: "2%",
  },
}));

export default function WhoIsIn() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.binBox}>
        <Grid container spacing={4}>
          <Grid item xs>
            <Paper className={classes.paper}>
              <Typography variant="h4">Current Date</Typography>
              <img className={classes.binImage} alt="Prescription Item -TBA" />
            </Paper>
          </Grid>
        </Grid>
        <Grid container spacing={4}>
          <Grid item xs>
            <Paper className={classes.paper}>
              <Typography variant="h5">Person A: In</Typography>
            </Paper>
          </Grid>
          <Grid item xs>
            <Paper className={classes.paper}>
              <Typography variant="h5">Person B: In</Typography>
            </Paper>
          </Grid>
          <Grid item xs>
            <Paper className={classes.paper}>
              <Typography variant="h5">Person C: Out</Typography>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
