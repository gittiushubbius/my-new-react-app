import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import genWaste from "../images/genWaste.png";
import recWaste from "../images/recWaste.png";
import garWaste from "../images/garWaste.png";
import nextDate from "../functions/nextDate";
import store from "../store/store.js";
import { getBinDates } from "../actions/actionTypes.js";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    paddingTop: 50,
  },
  paper: {
    padding: theme.spacing(4),
    textAlign: "center",
    color: theme.palette.text.secondary,
    border: "1px solid black",
  },
  binImage: {
    maxHeight: 50,
    paddingTop: 15,
  },
  alert: {
    color: "Red",
  },
  binBox: {
    marginLeft: "2%",
    marginRight: "2%",
  },
}));

export default function AutoGrid() {
  store.subscribe(() => store.getState());
  store.dispatch(getBinDates());

  const classes = useStyles();

  return (
    <div>
      <div className={classes.binBox}>
        <Grid container spacing={4} className={classes.root}>
          <Grid container direction="column" item xs spacing={2}>
            <Grid item>
              <Paper className={classes.paper}>
                <Typography variant="h4">Recycling</Typography>
                <img
                  className={classes.binImage}
                  src={recWaste}
                  alt="Recycling Waste Bin Icon"
                />
              </Paper>
            </Grid>
            <Grid item>
              <Paper className={classes.paper}>
                <Typography variant="h5">{nextDate("recycling")}</Typography>
              </Paper>
            </Grid>
          </Grid>
          <Grid container direction="column" item xs spacing={2}>
            <Grid item>
              <Paper className={classes.paper}>
                <Typography variant="h4">General</Typography>
                <img
                  className={classes.binImage}
                  src={genWaste}
                  alt="General Waste Bin Icon"
                />
              </Paper>
            </Grid>
            <Grid item>
              <Paper className={classes.paper}>
                <Typography variant="h5">{nextDate("landfill")}</Typography>
              </Paper>
            </Grid>
          </Grid>
          <Grid container direction="column" item xs spacing={2}>
            <Grid item>
              <Paper className={classes.paper}>
                <Typography variant="h4">Garden</Typography>
                <img
                  className={classes.binImage}
                  src={garWaste}
                  alt="Garden Waste Bin Icon"
                />
              </Paper>
            </Grid>
            <Grid item>
              <Paper className={classes.paper}>
                <Typography className={classes.alert} variant="h5">
                  {nextDate("garden")}
                </Typography>
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
