const binDayState = (
  state = {
    recycling: "2020-04-23",
    landfill: "2020-04-30",
    garden: "2020-04-45",
  },
  action
) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default binDayState;
