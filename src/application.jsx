import React from "react";
import Menu from "./components/menu/menu.jsx";
import Bins from "./components/binDay.jsx";
import PrescriptionChecker from "./components/prescriptions.jsx";
import WhoIsIn from "./components/whoIsIn.jsx";

class Application extends React.Component {
  constructor() {
    super();
    this.state = {
      services: [
        "When is my Bin Day?",
        "When is my Prescription Due?",
        "Who is in Today?",
      ],
      selected: 0,
    };
    this.select = this.select.bind(this);
  }

  select = (value) => {
    this.setState((state) => ({
      selected: value,
    }));
  };

  render() {
    function Service(props) {
      return (
        <div>
          {
            {
              0: <Bins></Bins>,
              1: <PrescriptionChecker></PrescriptionChecker>,
              2: <WhoIsIn></WhoIsIn>,
            }[props.selected]
          }
          }
        </div>
      );
    }

    return (
      <div>
        <div>
          <Menu
            selector={this.select}
            title={this.state.services[this.state.selected]}
          ></Menu>
        </div>
        <div>
          <Service selected={this.state.selected} />
        </div>
      </div>
    );
  }
}
export default Application;
