import { createStore } from "redux";
import binDayState from "../reducers/reducers.js";

const store = createStore(
  binDayState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
